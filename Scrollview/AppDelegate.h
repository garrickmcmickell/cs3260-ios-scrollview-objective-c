//
//  AppDelegate.h
//  Scrollview
//
//  Created by Garrick McMickell on 8/30/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

