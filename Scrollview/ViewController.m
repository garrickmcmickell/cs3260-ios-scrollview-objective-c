//
//  ViewController.m
//  Scrollview
//
//  Created by Garrick McMickell on 8/30/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:scrollView];
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3)];
    
    UIView *redView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    redView.backgroundColor = [UIColor redColor];
    [scrollView addSubview:redView];
    
    UIView *greenView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, 0, self.view.frame.size.width, self.view.frame.size.height)];
    greenView.backgroundColor = [UIColor greenColor];
    [scrollView addSubview:greenView];
    
    UIView *yellowView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    yellowView.backgroundColor = [UIColor yellowColor];
    [scrollView addSubview:yellowView];
    
    UIView *purpleView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    purpleView.backgroundColor = [UIColor purpleColor];
    [scrollView addSubview:purpleView];
    
    UIView *orangeView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    orangeView.backgroundColor = [UIColor orangeColor];
    [scrollView addSubview:orangeView];
    
    UIView *whiteView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:whiteView];
    
    UIView *blackView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
    blackView.backgroundColor = [UIColor blackColor];
    [scrollView addSubview:blackView];
    
    UIView *grayView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
    grayView.backgroundColor = [UIColor grayColor];
    [scrollView addSubview:grayView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
